#!/bin/bash

snakemake --use-conda --latency-wait 1000 -pr all --configfile ../../../config/config_bench_Atha.yaml --cores 16
