#!/usr/bin/env python

import argparse
import pandas as pd
import numpy as np
import logging
import os

## Class to handle coordinate conversion
#
class ConvCoord( object ):
    
    ## Constructor
    #
    def __init__( self ):

        self.cutting_fasta = ""
        self.pathFile = ""
        self.results_dir = ""
        self.chr = ""

        self.df_Chunks2CoordMaps = pd.DataFrame()
        self.df_path = pd.DataFrame()
        self.df_mergedPaths = pd.DataFrame()

        self.dict_chunkMap = {}
        

    def setAttributesFromCmdLine(self):
        parser = argparse.ArgumentParser(usage="ConvCoord.py [options]",
                                         description="Convert query coordinates into real coordinates (i.e. according to chromosome) and merge overlapping chunks at query coordinates.",
                                         epilog="z")

        parser.add_argument('-chr', '--chromosome', help= 'Chromosome file path', required= True)
        parser.add_argument('-fa_cut', '--cutting_fasta', help= 'Cutting fasta file from cutterDB', required= True, metavar='file')
        parser.add_argument('-path', '--path_file', help='Path filename from Matcher', required=True, metavar='file')
        args=parser.parse_args()
        self._setAttributesFromArguments(args)


    def _setAttributesFromArguments(self, args):

        self.chr = args.chromosome
        self.cutting_fasta = args.cutting_fasta
        self.pathFile = args.path_file

        logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO)
        logging.info("ConvCoord.py is running.")

        if not os.path.exists(self.cutting_fasta):
            logging.warning("No input file (cutting fasta file).")
        if not os.path.exists(self.cutting_fasta):
            logging.warning("No input file (path file).")


    ## From cutting fasta file, get map file
    #
    def getChunksCoordMap(self):
        with open(self.cutting_fasta, 'r') as text_input, open(self.cutting_fasta+f".map_{self.chr}", 'w') as text_output:
            for line in text_input:
                if line[0] == ">":
                    cutting = line.replace("..", " ").replace("  ", " ").split(" ")
                    #cutting = line.replace("..", "_").replace("__", "_").split("_")
                    chunk_name = cutting[0].replace(">", "")
                    query_name = cutting[1] #'_'.join(cutting[1:-2])
                    start =  cutting[2] #[-2]
                    end = cutting[3] #[-1]
                    if query_name == self.chr:
                        features = [chunk_name, query_name, start, end]
                        self.dict_chunkMap[chunk_name] = {"query_name" : query_name, "start" : start}
                        text_output.write("\t".join(features))



    ## Convert chunk coordinates, on path file from matcher
    #
    def convertChunksCoord(self):

        logging.info("Convert chunk coordinates is running...")
        with open(self.pathFile, 'r') as text_input, open(self.pathFile+".coordchr", 'w') as text_output:

            for line in text_input:
                
                cutting = line.split('\t')
                #print(cutting)
                path_number = cutting[0]
                chunk_name = cutting[1]
                query_start = cutting[2]
                query_end = cutting[3]
                others_features = cutting[4:]

                if chunk_name in self.dict_chunkMap.keys():
                    query_name = self.dict_chunkMap[chunk_name]["query_name"]
                elif chunk_name.split(' ')[0] in self.dict_chunkMap.keys(): #('_')
                    chunk_name = chunk_name.split(' ')[0] #('_')
                    query_name = self.dict_chunkMap[chunk_name]["query_name"]
                else:
                    logging.warning("Chunk name {} is not defined in {} file.".format( chunk_name, self.cutting_fasta+f".map_{self.chr}") )
                
                chunk_start = self.dict_chunkMap[chunk_name]["start"]

                real_query_start = int(query_start) + int(chunk_start) - 1
                real_query_end = int(query_end) + int(chunk_start) - 1

                features = [str(path_number), query_name, str(real_query_start), str(real_query_end)] + others_features

                text_output.write("\t".join(features))

        logging.info("Convert chunk coordinates is done.")


    ## Import chunk map file
    #
    def importChunkMapFile(self):

        self.df_Chunks2CoordMaps = pd.read_csv(self.cutting_fasta+f".map_{self.chr}", sep="\t", header=None, names=["chunk_number", "chr", "start", "end"], index_col=["chunk_number"])


    ## Import path file
    #
    def importPathFile(self, extension):

        df = pd.read_csv(self.pathFile+extension, sep="\t", header=None, names=["path_number", "query_name", "query_start", "query_end", "subject_name", "subject_start", "subject_end", "e_value", "score", "identity"])
        cols = ['query_start', 'query_end', 'subject_start', 'subject_end', 'identity']
        df[cols] = df[cols].round(3)

        return df


    ## In query coordinates (i.e chunks), give a dataframe of Path instances overlapping a given region
    #
    # @note whole chains are returned, even if only a fragment overlap with the given region
    # @return dataframe of Path instances
    #
    def getChainOverlappingQueryCoord(self, query_name, minCoord, maxCoord):

        #logging.info("getChainOverlappingQueryCoord is running.")

        if( minCoord > maxCoord ):
            minCoord, maxCoord = maxCoord, minCoord

        #self.df_path = self.importPathFile(".coordchr")

        df_path = self.df_path.loc[((self.df_path["query_name"] == query_name) 
                                    & ((self.df_path["query_start"] <= minCoord) & (self.df_path["query_end"] >= minCoord) & (self.df_path["query_end"] <= maxCoord)) 
                                    | ((self.df_path["query_start"] >= minCoord) & (self.df_path["query_end"] <= maxCoord))
                                    | ((self.df_path["query_start"] >= minCoord) & (self.df_path["query_start"] <= maxCoord) & (self.df_path["query_end"] >= maxCoord))
                                    | ((self.df_path["query_start"] <= minCoord) & (self.df_path["query_end"] >= maxCoord)))]

        #logging.info("getChainOverlappingQueryCoord is finished.")

        return df_path


    def getDirectAndReversePaths(self, df_Paths):

        df_DirectPaths = df_Paths.loc[((df_Paths["query_start"] <= df_Paths["query_end"]) & (df_Paths["subject_start"] <= df_Paths["subject_end"]))]
        df_ReversePaths = df_Paths.loc[((df_Paths["query_start"] > df_Paths["query_end"]) | (df_Paths["subject_start"] > df_Paths["subject_end"]))]

        return df_DirectPaths, df_ReversePaths


    ## Return True if the instance is on the direct strand, False otherwise
    # 
    def isOnDirectStrand(self, start, end):
        if start <= end:
            return True
        else:
            return False


    ## Return True if the instance overlaps with another Range instance, False otherwise
    #
    # @param range1 a range instance
    # @param range2 a range instance
    #
    def isOverlapping(self, start1, end1, start2, end2):

        smin = min(start1, end1)
        smax = max(start1, end1)
        omin = min(start2, end2)
        omax = max(start2, end2)
        if omin <= smin and omax >= smax:
            return True
        if omin >= smin and omin <= smax or omax >= smin and omax <= smax:
            return True
        return False


    ## Return True if the instance can be merged with another Path instance, False otherwise
    #
    # @param path1 a Path instance
    # @param path2 a Path instance
    #
    def canMerge(self, path1, path2):

        #logging.info("canMerge is running.")

        if (path1.path_number != path2.path_number) \
            and (path1.query_name == path2.query_name) \
            and (path1.subject_name == path2.subject_name) \
            and self.isOnDirectStrand(path1.query_start, path1.query_end) == self.isOnDirectStrand(path2.query_start, path2.query_end) \
            and self.isOnDirectStrand(path1.subject_start, path1.subject_end) == self.isOnDirectStrand(path2.subject_start, path2.subject_end) \
            and (self.isOverlapping(path1.query_start, path1.query_end, path2.query_start, path2.query_end)) \
            and (self.isOverlapping(path1.subject_start, path1.subject_end, path2.subject_start, path2.subject_end)):

            #logging.info("canMerge is finished.")

            return True

    
    ## Merge the instance with another Range instance
    #
    # @param o a Range instance
    #
    def mergeRange(self, start1, end1, start2, end2):

        if self.isOnDirectStrand(start1, end1):
            start = min(start1, start2)
            end = max(end1, end2)
        else:
            start = max(start1, start2)
            end = min(end1, end2)

        return start, end


    ## Merge the instance with another Align instance
    #
    # @param o an Align instance
    #
    def merge(self, df_Path, id1, id2):

        #logging.info("merge is running.")

        if (df_Path.loc[df_Path.path_number == id1, "subject_name"].values[0] != df_Path.loc[df_Path.path_number == id2, "subject_name"].values[0]):
            return

        query_start, query_end = self.mergeRange(df_Path.query_start.values[0], df_Path.query_end.values[0], df_Path.query_start.values[0], df_Path.query_end.values[0]) # Merge query range
        subject_start, subject_end = self.mergeRange(df_Path.subject_start.values[0], df_Path.subject_end.values[0], df_Path.subject_start.values[0], df_Path.subject_end.values[0]) # Merge subject range

        df_Path.loc[df_Path.path_number == id1, "query_start"].values[0] = query_start
        df_Path.loc[df_Path.path_number == id1, "query_start"].values[0] = query_start
        df_Path.loc[df_Path.path_number == id1, "query_end"].values[0] = query_end
        df_Path.loc[df_Path.path_number == id1, "subject_start"].values[0] = subject_start
        df_Path.loc[df_Path.path_number == id1, "subject_end"].values[0] = subject_end

        df_Path.loc[df_Path.path_number == id1, "e_value"] = min(df_Path.loc[df_Path.path_number == id1, "e_value"].values[0], df_Path.loc[df_Path.path_number == id2, "e_value"].values[0])
        df_Path.loc[df_Path.path_number == id1, "score"] = max(df_Path.loc[df_Path.path_number == id1, "score"].values[0], df_Path.loc[df_Path.path_number == id2, "score"].values[0])
        df_Path.loc[df_Path.path_number == id1, "identity"] = max(df_Path.loc[df_Path.path_number == id1, "identity"].values[0], df_Path.loc[df_Path.path_number == id2, "identity"].values[0])
        
        #logging.info("merge is finished.")

        return df_Path


    def mergePaths(self, df_Paths):

        if len(df_Paths) < 2:
            return df_Paths

        i = 0
        while i < len(df_Paths) - 1:
            i += 1
            idPrev = df_Paths.iloc[i-1].path_number
            idNext = df_Paths.iloc[i].path_number
            
            if self.canMerge(df_Paths.iloc[i-1], df_Paths.iloc[i]):
                df_Paths = self.merge(df_Paths, idPrev, idNext)
                df_Paths = df_Paths.drop(df_Paths[df_Paths.path_number == idNext].index)
                i -= 1
        
        return df_Paths

        
    ## Merge coordinates on chunk overlaps
    #
    def mergeCoordsOnChunkOverlaps(self):

        logging.info("Merge coordinates on chunk overlaps is running...")
        self.importChunkMapFile()
        l_chunks = self.df_Chunks2CoordMaps.index[:-1]

        self.df_path = self.importPathFile(".coordchr")
        self.df_mergedPaths = self.importPathFile(".coordchr")

        for chunk_number in l_chunks:
            chunkName2 = int(chunk_number)+1
            logging.info(f"Analyse overlapping between chunk{chunk_number} and chunk{chunkName2}:")

            chrName1 = self.df_Chunks2CoordMaps.loc[chunk_number]["chr"]
            chrName2 = self.df_Chunks2CoordMaps.loc[chunkName2]["chr"]

            if chrName2 != chrName1:
                msg = "not on same chromosome ({} != {})" .format ( chrName1, chrName2 )
                logging.info(msg)
            else:
                end_chunk1 = self.df_Chunks2CoordMaps.loc[chunk_number]["end"]
                start_chunk2 = self.df_Chunks2CoordMaps.loc[chunkName2]["start"]

                minCoord = min( end_chunk1, start_chunk2 )
                maxCoord = max( end_chunk1, start_chunk2 )

                df_Paths = self.getChainOverlappingQueryCoord( chrName1, minCoord, maxCoord )

                if len(df_Paths) == 0:
                    msg = "no overlapping matches on {} ({}->{})" .format ( chrName1, minCoord, maxCoord )
                    logging.info(msg)
                    
                else:
                    msg = "{} overlapping matche(s) on {} ({}->{})" .format ( len(df_Paths), chrName1, minCoord, maxCoord )
                    logging.info(msg)

                    # Remove current paths in order to update further (update i.e. potentially merge) 
                    self.df_mergedPaths = pd.concat([self.df_mergedPaths, df_Paths]).drop_duplicates(keep=False)

                    # Path instances sorted in increasing order according to the min of the query, then the max of the query, then their identifier, and finally their initial order
                    df_SortedPaths = df_Paths.sort_values(by=["query_start", "query_end", "query_name"])

                    df_DirectPaths, df_ReversePaths = self.getDirectAndReversePaths(df_SortedPaths)

                    if len(df_DirectPaths) > 0:
                        df_DirectPaths = self.mergePaths(df_DirectPaths)
                    
                    if len(df_ReversePaths) > 0:
                        df_ReversePaths = self.mergePaths(df_ReversePaths)

                    self.df_mergedPaths = pd.concat([self.df_mergedPaths, df_DirectPaths])
                    self.df_mergedPaths = pd.concat([self.df_mergedPaths, df_ReversePaths])
 
            
        self.df_mergedPaths.to_csv(self.pathFile+".coordchr.merged", sep="\t", header=False, index=False)
        





    ##############################################################################################################
    ## File conversions

    def addStrandinPathFile(self, df):

        df.loc[df["subject_end"] >= df["subject_start"], "strand"] = '+'
        df.loc[df["subject_end"] < df["subject_start"], "strand"] = '-'
        mask = df["strand"] == "-"
        # For minus strand, switch the subject start and the subject end
        df.loc[mask, ['subject_start', 'subject_end']] = (df.loc[mask, ['subject_end', 'subject_start']].values)

        return df


    #Get entire path from Matcher (or EP)
    def getEP(self, df_path):

        df = df_path.groupby(by="path_number").filter(lambda x : len(x) == 1)
        if df.empty:
                return df
        else:
                df["sub_path_number"] = 1
                return df


    #Get fragmented path from Matcher (or FP)
    def getFP(self, df_path):

        df = df_path.groupby(by="path_number").filter(lambda x : len(x) > 1)

        if df.empty:
                return df
        else:

                df['sub_path_number'] = df.groupby('path_number').cumcount() + 1
                #Calculate weighted %id/e-value
                df['HSP_len'] = df['query_end'] - df['query_start'] + 1 # HSP = High Scoring Pairs
                df_groupBy = df.groupby(by="path_number")
                df_groupSize = df_groupBy.size().reset_index(name='nb_of_subjects')
                df_lenAlignment = df_groupBy.apply(lambda x: sum(x['HSP_len'])).reset_index(name='HSP_totalLen')
                df = pd.merge(df, df_lenAlignment, on='path_number')
                df['HSP_coverage'] = df['HSP_len']/df['HSP_totalLen']
                df['weighted_identity'] = df['identity']* df['HSP_coverage']
                df['weighted_e_value'] = df['e_value']* df['HSP_coverage']
                df['weighted_score'] = df['score']* df['HSP_coverage']
                df = pd.merge(df_groupSize, df, on='path_number')

                return df


    def setAttributesForGFF(self, x, path_type, type):

        target = " ".join([x['subject_name'], str(x['subject_start']), str(x['subject_end'])])

        if type == "match" and path_type == "EP":
            return "ID={};Name={};Target={};Note=e-value:{},identity:{}".format(x['path_number'], x['subject_name'], target, x['e_value'], x['identity'])

        elif type == "match" and path_type == "FP":
            target_FP = " ".join([x['subject_name'], str(x['subject_start']), str(x['subject_end'])])
            return "ID={};Name={};Target={};Note=e-value:{},identity:{}".format(x['path_number'], x['subject_name'], target_FP, x['weighted_e_value'], x['weighted_identity'])

        elif type == "match_part":
            return "ID={};Parent={};Name={};Target={};Note=e-value:{},identity:{}".format(x['ID'], x['path_number'], x['subject_name'], target, x['e_value'], x['identity'])


    def getGFF3(self, df, path_type, type):

        if not df.empty:

            df["source"] = "matcher"
            df["type"] = type

            df["phase"] = "."

            if type == "match_part":
                df["ID"] = df["path_number"].astype(str) + "." + df["sub_path_number"].astype(str)
                df["ID"] = df["ID"].astype(float)
            elif type == "match":
                df["ID"] = df["path_number"].astype(float)
                if path_type == "FP":
                    df["score"] = df["weighted_score"]

            df["attributes"] = df.apply(lambda x: self.setAttributesForGFF(x, path_type, type), axis=1)

            df = df[["query_name", "source", "type", "query_start", "query_end", "score", "strand", "phase", "attributes", "ID"]]

            return df

        else:
            return df


    def path2gff3( self, df_EP, df_FP ):

        gff3_cols = ["query_name", "source", "type", "query_start", "query_end", "score", "strand", "phase", "attributes", "ID"]

        # Entire paths
        if not df_EP.empty:
                df_EP_match = self.getGFF3(df_EP, "EP", "match")
                df_EP_match_part = self.getGFF3(df_EP, "EP", "match_part")
                df_EP = pd.concat([df_EP_match, df_EP_match_part])
        else:
                df_EP = pd.DataFrame(columns=gff3_cols)


        # Fragmented paths
        if not df_FP.empty:
                df_FP_match = df_FP.groupby(by="path_number")
                df_FP_match = df_FP_match.agg({'query_name': 'first', 
                                                'query_start': ['min'], 
                                                'query_end': ['max'], 
                                                'subject_name': 'first',
                                                'subject_start': 'min',
                                                'subject_end': 'max',
                                                'weighted_e_value': ['sum'],
                                                'weighted_score': ['sum'],
                                                'weighted_identity': ['sum'],
                                                'strand': 'first'})
                df_FP_match.columns = ['query_name', 'query_start', 'query_end', 'subject_name', 'subject_start', 'subject_end', 'weighted_e_value', 'weighted_score', 'weighted_identity', 'strand']
                df_FP_match = df_FP_match.reset_index()
                df_FP_match = self.getGFF3(df_FP_match, "FP", "match")

                df_FP_match_part = self.getGFF3(df_FP, "FP", "match_part")

                df_FP = pd.concat([df_FP_match, df_FP_match_part])

        else:
                df_FP = pd.DataFrame(columns=gff3_cols)

        df_gff3 = pd.concat([df_EP, df_FP])
        df_gff3 = df_gff3.sort_values(by=["ID"])
        df_gff3 = df_gff3.drop(columns=["ID"])
        cols = ['query_start', 'query_end', 'score']
        df_gff3[cols] = df_gff3[cols].applymap(np.int64)


        df_gff3.to_csv(self.pathFile+".coordchr.merged.gff3", sep="\t", header=False, index=False)


    def path2bed( self, df_path ):

        bed_cols = ["query_name", "query_start", "query_end", "subject_name", "score", "strand", "subject_start", "subject_end", "identity", "nb_of_subjects"]

        df_path["query_start"] = df_path["query_start"] -1
        df_path["query_end"] = df_path["query_end"] -1
        df_path["subject_start"] = df_path["subject_start"] -1
        df_path["subject_end"] = df_path["subject_end"] -1
        df_path["subject_name"] = df_path["path_number"].astype(str) + '_' + df_path["subject_name"]

        # Entire paths
        df_EP = self.getEP(df_path)
        if not df_EP.empty:
                        df_EP["nb_of_subjects"] = 1
                        df_EP = df_EP[bed_cols]
        else:
                df_EP = pd.DataFrame(columns=bed_cols)

        # Fragmented paths
        df_FP = self.getFP(df_path)
        if not df_FP.empty:
                df_FP = df_FP.groupby(by="path_number").agg({'query_name': 'first', 
                                                'query_start': 'min', 
                                                'query_end': 'max', 
                                                'subject_name': lambda x: ','.join(sorted(x)),
                                                'weighted_score' : lambda x: sum(x),
                                                'strand': 'first',
                                                'subject_start': lambda x: ','.join(map(str, x)), 
                                                'subject_end': lambda x: ','.join(map(str, x)), 
                                                'weighted_identity' : lambda x: sum(x),
                                                'nb_of_subjects': 'first'})
                df_FP = df_FP.reset_index()
                df_FP = df_FP.rename(columns={'weighted_identity': 'identity', 'weighted_score': 'score'})
        else:
                df_FP = pd.DataFrame(columns=bed_cols)

        df_bed = pd.concat([df_EP, df_FP])
        df_bed = df_bed.sort_values(by=["query_name", "query_start", "query_end", "subject_name"])

        df_bed.to_csv(self.pathFile+".coordchr.merged.bed", sep="\t", header=False, index=False)




    def run( self ):

        if os.path.getsize(self.pathFile) == 0:
                with open(self.pathFile+".coordchr", "w") as file:
                           pass
                with open(self.pathFile+".coordchr.merged", "w") as file:
                           pass
        else:
                self.getChunksCoordMap()
                self.convertChunksCoord()
                self.mergeCoordsOnChunkOverlaps()
                logging.info("Merge coordinates on chunk overlaps is done.")

        if os.path.getsize(self.pathFile+".coordchr.merged") == 0:
                with open(self.pathFile+".coordchr.merged.gff3", "w") as file:
                           pass
                with open(self.pathFile+".coordchr.merged.bed", "w") as file:
                           pass
        else:

                df_path = self.importPathFile(".coordchr.merged")
                df_path = self.addStrandinPathFile(df_path)
                df_EP = self.getEP(df_path)
                df_FP = self.getFP(df_path)

                logging.info("Conversion path2gff3 is running...")
                self.path2gff3( df_EP, df_FP )
                logging.info("Conversion path2gff3 is done.")

                logging.info("Conversion path2bed is running...")
                self.path2bed( df_path )
                logging.info("Conversion path2bed is done.")


if __name__ == '__main__':
    i = ConvCoord()
    i.setAttributesFromCmdLine()
    i.run()
    logging.info("ConvCoord is done.")
