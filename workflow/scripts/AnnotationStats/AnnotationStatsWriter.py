#!/usr/bin/env python

from Stat import Stat

class AnnotationStatsWriter(object):

    def __init__(self):
        self._dAllTErefseqs = { "sumCumulCoverage": 0,
                         "totalNbFragments": 0,
                         "totalNbFullLengthFragments": 0,
                         "totalNbCopies": 0,
                         "totalNbFullLengthCopies": 0,
                         "nbFamWithFullLengthFragments": 0,
                         "nbFamWithOneFullLengthFragment": 0,
                         "nbFamWithTwoFullLengthFragments": 0,
                         "nbFamWithThreeFullLengthFragments": 0,
                         "nbFamWithMoreThanThreeFullLengthFragments": 0,
                         "nbFamWithFullLengthCopies": 0,
                         "nbFamWithOneFullLengthCopy": 0,
                         "nbFamWithTwoFullLengthCopies": 0,
                         "nbFamWithThreeFullLengthCopies": 0,
                         "nbFamWithMoreThanThreeFullLengthCopies": 0,
                         "statsAllCopiesMedIdentity": Stat(),
                         "statsAllCopiesMedLengthPerc": Stat()
                         }


    def getAllTEsRefSeqDict(self):
        return self._dAllTErefseqs

    def getStatAsStringForTE(self, name, d):
        """
        Return a string with all data properly formatted.
        """

        string = ""

        #string += d["Wcode"]
        string += ""

        string += "\t{}" .format(name)
        string += "\t{}" .format(d["length"])
        string += "\t{}" .format(d["cumulCoverage"])
        string += "\t{}" .format(d["nbFragments"])
        string += "\t{}" .format(d["nbFullLengthFragments"])
        string += "\t{}" .format(d["nbCopies"])
        string += "\t{}" .format(d["nbFullLengthCopies"])

        if d["statsIdentityPerChain"].getValuesNumber() != 0:
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].mean())

            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].sd())
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].getMin())
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].quantile(0.25))
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].median())
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].quantile(0.75))
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].getMax())

        else:
            for i in range(0, 7):
                string += "\tNA"

        if d["statsLengthPerChain"].getValuesNumber() != 0:
            string += "\t{:.2f}" .format(d["statsLengthPerChain"].mean())

            string += "\t{:.2f}" .format(d["statsLengthPerChain"].sd())
            string += "\t{}" .format(int(d["statsLengthPerChain"].getMin()))
            string += "\t{:.2f}" .format(d["statsLengthPerChain"].quantile(0.25))
            string += "\t{:.2f}" .format(d["statsLengthPerChain"].median())
            string += "\t{:.2f}" .format(d["statsLengthPerChain"].quantile(0.75))
            string += "\t{}" .format(int(d["statsLengthPerChain"].getMax()))
        else:
            for i in range(0, 7):
                string += "\tNA"

        if d["statsLengthPerChainPerc"].getValuesNumber() != 0:
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].mean())

            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].sd())
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].getMin())
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].quantile(0.25))
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].median())
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].quantile(0.75))
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].getMax())
        else:
            for i in range(0, 7):
                string += "\tNA"

        return string

    def getStatAsStringForCluster(self, name, d):
        """
        Return a string with all data properly formatted.
        """
        string = ""
        string += "{}" .format(name)

        string += "\t{}" .format(d["cumulCoverage"])
        string += "\t{}" .format(d["nbFragments"])
        string += "\t{}" .format(d["nbCopies"])

        return string

    def addCalculsOfOneTE(self, dOneTErefseq):
        self._dAllTErefseqs[ "sumCumulCoverage" ] += dOneTErefseq[ "cumulCoverage" ]
        self._dAllTErefseqs[ "totalNbFragments" ] += dOneTErefseq[ "nbFragments" ]
        self._dAllTErefseqs[ "totalNbFullLengthFragments" ] += dOneTErefseq[ "nbFullLengthFragments" ]
        if dOneTErefseq[ "nbFullLengthFragments" ] > 0:
            self._dAllTErefseqs[ "nbFamWithFullLengthFragments" ] += 1
        if dOneTErefseq[ "nbFullLengthFragments" ] == 1:
            self._dAllTErefseqs[ "nbFamWithOneFullLengthFragment" ] += 1
        elif dOneTErefseq[ "nbFullLengthFragments" ] == 2:
            self._dAllTErefseqs[ "nbFamWithTwoFullLengthFragments" ] += 1
        elif dOneTErefseq[ "nbFullLengthFragments" ] == 3:
            self._dAllTErefseqs[ "nbFamWithThreeFullLengthFragments" ] += 1
        elif dOneTErefseq[ "nbFullLengthFragments" ] > 3:
            self._dAllTErefseqs[ "nbFamWithMoreThanThreeFullLengthFragments" ] += 1

        self._dAllTErefseqs[ "totalNbCopies" ] += dOneTErefseq[ "nbCopies" ]
        self._dAllTErefseqs[ "totalNbFullLengthCopies" ] += dOneTErefseq[ "nbFullLengthCopies" ]
        if dOneTErefseq[ "nbFullLengthCopies" ] > 0:
            self._dAllTErefseqs[ "nbFamWithFullLengthCopies" ] += 1
        if dOneTErefseq[ "nbFullLengthCopies" ] == 1:
            self._dAllTErefseqs[ "nbFamWithOneFullLengthCopy" ] += 1
        elif dOneTErefseq[ "nbFullLengthCopies" ] == 2:
            self._dAllTErefseqs[ "nbFamWithTwoFullLengthCopies" ] += 1
        elif dOneTErefseq[ "nbFullLengthCopies" ] == 3:
            self._dAllTErefseqs[ "nbFamWithThreeFullLengthCopies" ] += 1
        elif dOneTErefseq[ "nbFullLengthCopies" ] > 3:
            self._dAllTErefseqs[ "nbFamWithMoreThanThreeFullLengthCopies" ] += 1

        if dOneTErefseq[ "statsIdentityPerChain" ].getValuesNumber() != 0:
            self._dAllTErefseqs[ "statsAllCopiesMedIdentity" ].add(dOneTErefseq[ "statsIdentityPerChain" ].median())

        if dOneTErefseq[ "statsLengthPerChainPerc" ].getValuesNumber() != 0:
            self._dAllTErefseqs[ "statsAllCopiesMedLengthPerc" ].add(dOneTErefseq[ "statsLengthPerChainPerc" ].median())

    def printStatsForAllTEs(self, TEnb):
        statString = "total nb of TE fragments: {}\n" .format(self._dAllTErefseqs[ "totalNbFragments" ])

        if self._dAllTErefseqs[ "totalNbFragments" ] != 0:

            statString += "total nb full-length fragments: {} ({:.2f}%)\n" .format \
            (self._dAllTErefseqs[ "totalNbFullLengthFragments" ], \
              100 * self._dAllTErefseqs[ "totalNbFullLengthFragments" ] / float(self._dAllTErefseqs[ "totalNbFragments" ]))

            statString += "total nb of TE copies: {}\n" .format(self._dAllTErefseqs[ "totalNbCopies" ])

            statString += "total nb full-length copies: {} ({:.2f}%)\n" .format \
            (self._dAllTErefseqs[ "totalNbFullLengthCopies" ], \
              100 * self._dAllTErefseqs[ "totalNbFullLengthCopies" ] / float(self._dAllTErefseqs[ "totalNbCopies" ]))

            statString += "families with full-length fragments: {} ({:.2f}%)\n" .format \
            (self._dAllTErefseqs[ "nbFamWithFullLengthFragments" ], \
              100 * self._dAllTErefseqs[ "nbFamWithFullLengthFragments" ] / float(TEnb))
            statString += " with only one full-length fragment: {}\n" .format(self._dAllTErefseqs[ "nbFamWithOneFullLengthFragment" ])
            statString += " with only two full-length fragments: {}\n" .format(self._dAllTErefseqs[ "nbFamWithTwoFullLengthFragments" ])
            statString += " with only three full-length fragments: {}\n" .format(self._dAllTErefseqs[ "nbFamWithThreeFullLengthFragments" ])
            statString += " with more than three full-length fragments: {}\n" .format(self._dAllTErefseqs[ "nbFamWithMoreThanThreeFullLengthFragments" ])

            statString += "families with full-length copies: {} ({:.2f}%)\n" .format \
            (self._dAllTErefseqs[ "nbFamWithFullLengthCopies" ], \
              100 * self._dAllTErefseqs[ "nbFamWithFullLengthCopies" ] / float(TEnb))
            statString += " with only one full-length copy: {}\n" .format(self._dAllTErefseqs[ "nbFamWithOneFullLengthCopy" ])
            statString += " with only two full-length copies: {}\n" .format(self._dAllTErefseqs[ "nbFamWithTwoFullLengthCopies" ])
            statString += " with only three full-length copies: {}\n" .format(self._dAllTErefseqs[ "nbFamWithThreeFullLengthCopies" ])
            statString += " with more than three full-length copies: {}\n" .format(self._dAllTErefseqs[ "nbFamWithMoreThanThreeFullLengthCopies" ])

            statString += "mean of median identity of all families: {:.2f} +- {:.2f}\n" .format \
            (self._dAllTErefseqs[ "statsAllCopiesMedIdentity" ].mean(), \
              self._dAllTErefseqs[ "statsAllCopiesMedIdentity" ].sd())

            statString += "mean of median length percentage of all families: {:.2f} +- {:.2f}\n" .format \
            (self._dAllTErefseqs[ "statsAllCopiesMedLengthPerc" ].mean(), \
              self._dAllTErefseqs[ "statsAllCopiesMedLengthPerc" ].sd())
        return statString

    def printResume(self, lNamesTErefseq, lDistinctSubjects, totalCumulCoverage, genomeLength):
        statString = "nb of sequences: {}\n" .format(len(lNamesTErefseq))
        statString += "nb of matched sequences: {}\n" .format(len(lDistinctSubjects))
        statString += "genome length (bp): {}\n" .format(genomeLength)
        statString += "cumulative coverage (bp): {}\n" .format(totalCumulCoverage)
        statString += "coverage percentage: {:.2f}%\n" .format(100 * totalCumulCoverage / float(genomeLength))
        return statString
