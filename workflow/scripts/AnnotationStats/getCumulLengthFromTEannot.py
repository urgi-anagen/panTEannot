#!/usr/bin/env python

import subprocess
import sys
import os

import numpy as np
from Bio.SeqIO.FastaIO import SimpleFastaParser
import pandas as pd

import logging

class getCumulLengthFromTEannot(object):
    """
    Give the cumulative length of TE annotations (subjects mapped on queries).
    """

    def __init__(self):
        self._tableName = ""
        self._TErefseq = ""

    def setGenome(self, genome):
        self._genomeFilename = genome

    def setInputTable(self, inTableName, inTable):
        self._pathFilename = inTableName
        self._df_path = inTable

    def setTErefseq(self, a):
        self._TErefseq = a


    # Sources:
    # self._tpA's methods from https://forgemia.inra.fr/urgi-anagen/repet_pipe/-/blob/dev/commons/core/sql/TablePathAdaptator.py
    # lPaths's method like getSubjectAsMapOfQuery() from https://forgemia.inra.fr/urgi-anagen/repet_pipe/-/blob/dev/commons/core/coord/Align.py
    # map attributes : https://forgemia.inra.fr/urgi-anagen/repet_pipe/-/blob/dev/commons/core/coord/Map.py
    def getAllSubjectsAsBedOfQueries(self):

        if self._TErefseq == "":
            bedFileName = "{}.bed2".format(self._pathFilename)
        else:
            bedFileName = "{}.bed2_oneTE".format(self._pathFilename)
            self._df_path = self._df_path.loc[self._df_path["subject_name"] == self._TErefseq]

        self._df_path.loc[self._df_path["subject_end"] >= self._df_path["subject_start"], "strand"] = '+'
        self._df_path.loc[self._df_path["subject_end"] < self._df_path["subject_start"], "strand"] = '-'
        mask = self._df_path["strand"] == "-"
        # For minus strand, switch the subject start and the subject end
        self._df_path.loc[mask, ['subject_start', 'subject_end']] = (self._df_path.loc[mask, ['subject_end', 'subject_start']].values)

        cols = ["query_name", "query_start", "query_end", "subject_name"]
        self._df_path[["query_start", "query_end"]] = self._df_path[["query_start", "query_end"]].applymap(np.int64)
        df_bed = self._df_path[cols]

        df_bed = df_bed.sort_values(by=cols)
        df_bed.to_csv(bedFileName, sep="\t", header=False, index=False)
        
        return bedFileName


    ## Bedtools genomecov need genome in map file
    #
    def getGenomeMapFile(self):
        
        #genomeMapFilename = self._genomeFilename + ".map"
        genomeMapFilename = os.path.dirname( self._pathFilename ) + '/' + os.path.basename( self. _genomeFilename ) + ".map"

        genomeFastaFile = open(self._genomeFilename, 'r')
        genomeMapFile = open(genomeMapFilename, 'w')

        for name, seq in SimpleFastaParser(genomeFastaFile):
            seqLen = len(seq)
            #name = name.replace(",", "").replace("|", "").replace(":", "").replace(" ", "")
            genomeMapFile.write(name + '\t' + str(seqLen) + '\n')

        genomeFastaFile.close()
        genomeMapFile.close()

        return genomeMapFilename


    ## Perform bedtools genomecov
    #
    def genomecov(self):
        
        bedFileName = self.getAllSubjectsAsBedOfQueries()
        genomeMapFilename = self.getGenomeMapFile()

        genomecovFileName = "{}.genomecov".format(bedFileName)

        prg = "bedtools genomecov"
        cmd = prg
        cmd += " -i {}".format(bedFileName)
        cmd += " -g {}".format(genomeMapFilename)
        cmd += " > {}".format(genomecovFileName)

        log = subprocess.call(cmd, shell=True)
        if log != 0:
            logging.error("*** Error: {} returned {}".format(prg, log))

        return genomecovFileName


    ## Retrieve genome coverage and genome length from bedtools genomecov output
    #
    def getCumulLength(self):
        
        genomecovFileName = self.genomecov()

        with open(genomecovFileName, 'r') as text:
            for line in text:
                if "genome\t0\t" in line:
                    features = line.rstrip().split("\t")
                    covsize = features[2]
                    gensize = features[3]

        return int(gensize)-int(covsize), int(gensize)
