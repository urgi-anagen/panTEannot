#TEannotSnakemake_AthaGenome_athaTEref_ru_S0.gff3

Genome: AthaGenome.fa
TE library : athaTEref_ru1.fasta
cutterDB params: -l 200000 -o 10000
Blaster from TE_finder: te_finder_2.30.2.sif
matcher: matcher2.31.exe
Blaster params: -X -S 0 -l 2000000 -p '-num_threads 2' -r -v 1
Matcher params: -j

#TEannotSnakemake_AthaGenome_athaTEref_ru_S1.gff3

Genome: AthaGenome.fa
TE library : athaTEref_ru1.fasta
cutterDB params: -l 200000 -o 10000
Blaster from TE_finder: te_finder_2.30.2.sif
matcher: matcher2.31.exe
Blaster params: -X -S 1 -l 2000000 -p '-num_threads 2' -r -v 1
Matcher params: -j

#TEannotSnakemake_AthaGenome_athaTEref_ru_S2.gff3

Genome: AthaGenome.fa
TE library : athaTEref_ru1.fasta
cutterDB params: -l 200000 -o 10000
Blaster from TE_finder: te_finder_2.30.2.sif
matcher: matcher2.31.exe
Blaster params: -X -S 2 -l 2000000 -p '-num_threads 2' -r -v 1
Matcher params: -j

#TEannotSnakemake_AthaGenome_ColO_refTEs_S0.gff3

Genome: AthaGenome.fa
TE library : Col0_refTEs.fa
cutterDB params: -l 200000 -o 10000
Blaster from TE_finder: te_finder_2.30.2.sif
matcher: matcher2.31.exe
Blaster params: -X -S 3 -l 2000000 -p '-num_threads 2' -r -v 1
Matcher params: -j
