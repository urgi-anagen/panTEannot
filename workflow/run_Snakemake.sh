#!/bin/bash

snakemake -pr all --configfile config/test.yaml --use-singularity --singularity-args '--bind $HOME' --rerun-incomplete -j --max-jobs-per-second 1
