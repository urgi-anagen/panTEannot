#!/usr/bin/env python

import argparse
import os
import logging

class RMcat2align( object ):
    
    ## Constructor
    #
    def __init__( self ):

        self.input = ""
        self.output = ""

    def setAttributesFromCmdLine(self):
        parser = argparse.ArgumentParser(usage="RMcat2align.py [options]",
                                         description="Convert the output file from RepeatMasker ('cat' format) into the 'align' format.")

        parser.add_argument('-i', '--input', help= 'Cat file from RepeatMasker', required= True, metavar='file')
        args=parser.parse_args()
        self._setAttributesFromArguments(args)


    def _setAttributesFromArguments(self, args):
        self.input = args.input
        self.output = self.input + ".align"


    def main(self):
        """
        Convert the output file from RepeatMasker ('cat' format) into the 'align' format.
        """

        verbose = 0

        logging.basicConfig(filename='RMcat2align.log', filemode='w', format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO)
        logging.info("RMcat2align is running")

        if not os.path.exists(self.input):
            logging.warning("No input file ('cat' format).")

        inFileHandler = open( self.input, "r" )
        outFileHandler = open( self.output, "w" )
        countLines = 0

        while True:
            line = inFileHandler.readline()
            if line == "":
                break
            countLines += 1

            tokens = line.split()
            try:
                test = int(line[0])
            except ValueError:
                continue

            scoreSW = tokens[0]
            percDiv = tokens[1]
            percId = 100.0 - float(percDiv)
            percDel = tokens[2]
            percIns = tokens[3]

            # query coordinates are always direct (start<end)
            qryName = tokens[4]
            qryStart = int(tokens[5])
            qryEnd = int(tokens[6])
            qryAfterMatch = tokens[7]
            
            # if subject on direct strand
            if len(tokens) == 13:
                sbjName = tokens[8]
                sbjStart = int(tokens[9])
                sbjEnd = int(tokens[10])
                sbjAfterMatch = tokens[11]
                
            # if subject on reverse strand
            elif tokens[8] == "C":
                sbjName = tokens[9]
                sbjAfterMatch = tokens[10]
                sbjStart = int(tokens[11])
                sbjEnd = int(tokens[12])
                
            # compute a new score: match length on query times identity
            matchLengthOnQuery = qryEnd - qryStart + 1
            newScore = int( matchLengthOnQuery * float(percId) / 100.0 )
            
            string = "{}\t{}\t{}\t{}\t{}\t{}\t0.0\t{}\t{}\n" .format( qryName, qryStart, qryEnd, sbjName, sbjStart, sbjEnd, newScore, percId )
            
            outFileHandler.write( string )
            
        inFileHandler.close()
        outFileHandler.close()
        logging.info("RMcat2align is finished")
        
        if verbose > 0:
            logging.info("nb of lines: {}").format( countLines )
        return 0

if __name__ == "__main__":
    i = RMcat2align()
    i.setAttributesFromCmdLine()
    i.main()
