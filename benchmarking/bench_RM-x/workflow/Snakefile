import os

# Data
genome = config["genome"]
genome_name = os.path.basename(config["genome"])
#TE_ref = os.path.basename(config["TE_ref"])
TE_ref = config["TE_ref"]

# Tool
RM = config["container"]["RM"]
#matcher = config["executable"]["matcher"] # matcher 2.31
TE_finder = config["container"]["TE_finder"] # matcher 2.30

# Folders
results_dir = "../results"
RM_dir = "{}/RM".format(results_dir)
chunk_dir = "{}/chunk_dir".format(results_dir)
log_dir = "{}/log".format(results_dir)
annotStats_dir = "{}/annotStats".format(results_dir)

matcher_Suffix = ["param", "bed", "map", "gff3", "path.attr"]
RM_Suffix = ["masked", "ori.out", "tbl"]


rule all:
    input:
        "{}/RM.fa_cut.cat.align.clean_match.path.total.out.coordchr.merged.gff3".format(results_dir),
        "{}/RM.fa_cut.cat.align.clean_match.path.total.out.coordchr.merged.bed".format(results_dir)
        #"{}/RM.fa_cut.cat.align.clean_match.path.total.out.coordchr.merged".format(annotStats_dir) + ".annotStatsPerTE.tab",
        #"{}/RM.fa_cut.cat.align.clean_match.path.total.out.coordchr.merged".format(annotStats_dir) + ".globalAnnotStatsPerTE.txt"


'''
rule cutterDB:
    input: genome
    output: 
        fa_cut = "{}/{}_cut".format(results_dir, genome_name),
        Nstretch_map = temp(genome + ".Nstretch.map")
    params:
        sif  = config["container"]["TE_finder"], 
        outdir = results_dir, 
        cutterDB_params = "-l 200000 -o 10000"
    log: "{}/cutterDB.log".format(log_dir)
    shell: """
            export SINGULARITY_BIND="/mnt" ;
            singularity exec {params.sif} cutterDB {input} {params.cutterDB_params} ;
            mv {input}_cut {params.outdir} ;
            sed -i 's/{{Cut}} //' {output.fa_cut}
            &> {log}
            """
'''
rule splitChunk:
    input: "{}/{}_cut".format(results_dir, genome_name)
    output: dynamic("{}/{{CHUNK}}.fa_cut".format(chunk_dir))
    params: outdir = chunk_dir
    shell : """
                awk -v outdir={params.outdir} '{{if (substr($0, 1, 1)==">") {{split(substr($0, 2),a," ");filename=a[1]".fa_cut"}}print $0  > outdir"/"filename}}' {input}
            """

rule repeatMasker:
    input:
        TE = TE_ref,
        chunk = "{}/{{CHUNK}}.fa_cut".format(chunk_dir)
    output:
        cat = "{}/{{CHUNK}}.fa_cut.cat".format(RM_dir)
        #others = temp(expand("{}/{{{{CHUNK}}}}.fa_cut.{{suffix}}".format(RM_dir), suffix=RM_Suffix))
    params: 
        RM = RM,
        RM_params = "-cutoff 200 -pa 1 -gccalc -no_is -nolow -s -e ncbi",
        outdir = RM_dir
    log: "{}/{{CHUNK}}-RM.log".format(log_dir)
    shell: """
                export SINGULARITY_BIND="/mnt" ;
                singularity exec {params.RM} RepeatMasker -q {input.chunk} -lib {input.TE} {params.RM_params} -dir {params.outdir} &> {log}
            """

rule RMcat2align:
    input:
        rules.repeatMasker.output.cat
    output:
        rules.repeatMasker.output.cat + ".align"
    params: RM_output = dynamic(expand("{}/{{{{CHUNK}}}}.fa_cut.{{suffix}}".format(RM_dir), suffix=RM_Suffix))
    log: "{}/{{CHUNK}}-RMcat2align.log".format(log_dir)
    shell: """
                python3 ../../../workflow/scripts/RMcat2align.py -i {input} &> {log}
                rm -f {params.RM_output}
            """

rule matcher:
    input:
        rules.RMcat2align.output
    output: 
        path = "{}/{{CHUNK}}.fa_cut.cat.align.clean_match.path".format(RM_dir)
    params:
        #matcher = matcher,
        TE_finder = TE_finder,
        others = temp(expand("{}/{{{{CHUNK}}}}.cat.align.clean_match.{{suffix}}".format(RM_dir), suffix=matcher_Suffix))
    log: "{}/matcher_{{CHUNK}}.log".format(log_dir)
    shell: """
                export SINGULARITY_BIND="/mnt" ;
                singularity exec {params.TE_finder} matcher2.30 -m {input} -j &> {log} ;
                touch {output} # The BLR-RM-x output can be empty
            """

rule concatenation:
    input: dynamic(rules.matcher.output)
    output: "{}/RM.fa_cut.cat.align.clean_match.path.total".format(results_dir)
    params: 
        TE_ref = config["TE_ref"]
    shell: """
            cat {input} >> {output}
            rm -f {params.TE_ref}_* {params.TE_ref}.*
            """

rule pathnum2id:
    input: rules.concatenation.output
    output: "{}/RM.fa_cut.cat.align.clean_match.path.total.out".format(results_dir)
    params: te_finder  = config["container"]["TE_finder"],
    log: "{}/pathnum2id.log".format(log_dir)
    shell: """
                export SINGULARITY_BIND="/mnt" ;
                singularity exec {params.te_finder} pathnum2id -i {input} &> {log}"
            """
    
rule convertCoord:
    input:
        fa_cut = "{}/{}_cut".format(results_dir, genome_name), 
        path = "{}/RM.fa_cut.cat.align.clean_match.path.total.out".format(results_dir)
    output:
        "{}/RM.fa_cut.cat.align.clean_match.path.total.out.coordchr.merged.gff3".format(results_dir),
        "{}/RM.fa_cut.cat.align.clean_match.path.total.out.coordchr.merged.bed".format(results_dir)
    log: "{}/convCoord.log2".format(log_dir)
    shell: "python3 ../../../workflow/scripts/convCoord.py -fa_cut {input.fa_cut} -path {input.path} -log {log} &> {log}"
    

'''
################################################################################################################################
# Compute statistics

rule annotationStats:
    input: 
        genome = config["genome"],
        TElibrary = config["TE_ref"],
        path = "{}/RM.fa_cut.cat.align.clean_match.path.total.out.coordchr.merged".format(results_dir)
    output: 
        annotStats = "{}/RM.fa_cut.cat.align.clean_match.path.total.out.coordchr.merged".format(annotStats_dir) + ".annotStatsPerTE.tab",
        globalAnnotStats = "{}/RM.fa_cut.cat.align.clean_match.path.total.out.coordchr.merged".format(annotStats_dir) + ".globalAnnotStatsPerTE.txt"
    log: "{}/AnnotationStats.log2".format(log_dir)
    shell: "python3 ../../../workflow/scripts/AnnotationStats/AnnotationStats.py -genome {input.genome} -TElibrary {input.TElibrary} -path {input.path} -annotStats {output.annotStats} -globalAnnotStats {output.globalAnnotStats} &> {log}"
'''
