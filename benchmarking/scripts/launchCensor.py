#!/usr/bin/env python

import sys
import os
import subprocess
import shutil

censor_dir = sys.argv[1]
tool = sys.argv[2]
input_chunk = sys.argv[3] # query
input_TE = sys.argv[4] # subject
censor_params = "-debug -s -ns -nofilter -bprm '-cpus 1' -noisy"
censorPerChunk_dir = os.path.basename(input_chunk)

#print(censor_dir)
if not os.path.exists(censor_dir):
    os.mkdir(censor_dir)
os.chdir(censor_dir)
#print(os.getcwd())

#print(censorPerChunk_dir)
if os.path.exists(censorPerChunk_dir):
    shutil.rmtree(censorPerChunk_dir)

os.mkdir(censorPerChunk_dir)
os.chdir(censorPerChunk_dir)
#print(os.getcwd())

os.symlink("../../" + input_chunk, os.path.basename(input_chunk))
#print("../../" + input_chunk, os.path.basename(input_chunk))
os.symlink(input_TE, os.path.basename(input_TE))
#print(input_TE, os.path.basename(input_TE))


cmd = "singularity exec {} censor {} -lib {} {}".format(tool, os.path.basename(input_chunk), os.path.basename(input_TE), censor_params)
print(cmd)
print(censor_params)
subprocess.call(cmd, shell=True)

output = "../" + censorPerChunk_dir + ".map"

if os.path.exists(censorPerChunk_dir + ".map"):
    shutil.copyfile(censorPerChunk_dir + ".map" , output)
else:
    subprocess.call(f"touch {output}", shell=True)

os.chdir("../")
shutil.rmtree(censorPerChunk_dir)

