#!/usr/bin/env python

import sys
import os
import subprocess
import shutil

blaster_dir = sys.argv[1]
blaster_version = sys.argv[2]
input_chunk = sys.argv[3] # query
input_TE = sys.argv[4] # subject
sensitivity = sys.argv[5]
blasterPerChunk = sys.argv[6] # -B option

blaster_params = f"-X -S {sensitivity} -l 2000000 -r -v 1"

blasterPerChunk_basename = os.path.basename(blasterPerChunk)

# Create temp directory for each chunk, to avoid conflict

if not os.path.exists(blaster_dir):
    os.mkdir(blaster_dir)

if os.path.exists(blasterPerChunk):
    shutil.rmtree(blasterPerChunk)

os.mkdir(blasterPerChunk)

shutil.copyfile( input_chunk, os.path.join(blasterPerChunk, os.path.basename(input_chunk)) )
os.symlink( input_TE, os.path.join(blasterPerChunk, os.path.basename(input_TE)) )

os.chdir(blasterPerChunk)

cmd = "{} -q {} -s {} {} -B {}".format(blaster_version, os.path.basename(input_chunk), os.path.basename(input_TE), blaster_params, blasterPerChunk_basename)
print(cmd)
subprocess.call(cmd, shell=True)

# Move the align file to the parent directory and remove the temp directory
shutil.copyfile(blasterPerChunk_basename + ".align", "../" + blasterPerChunk_basename + ".align")
os.chdir("../")
shutil.rmtree(blasterPerChunk_basename)
