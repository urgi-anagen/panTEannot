#!/usr/bin/env python

import unittest
from Bio import SeqIO

genomeFile = "../../ressources/AthaGenome.fa"
chunkFile = "../../results/AthaGenome-partial.fa_cut"
pathFile = "../../results/run_blaster-S0.align.clean_match.path.total.out"


## Check coordinate conversion (From chunk coordinates to genome coordinates) 
#
class Test_Coord(unittest.TestCase):

    def parseFasta(self, filename):

        record_dict = SeqIO.to_dict(SeqIO.parse(filename, "fasta"))

        return record_dict


    def test_compareExtractedSeqFromCoordinates(self):

        dictGenome = self.parseFasta(genomeFile)
        dictChunks = self.parseFasta(chunkFile)

        with open(pathFile, 'r') as Text, open(pathFile+".coordchr", 'r') as TextCoordchr:
            
            text = Text.readlines()
            textCoordchr = TextCoordchr.readlines()

            seq_list = []
            seqCoordChr_list = []

            for i in range(len(text)):

                # Coordinates according to chunk
                cutting = text[i].split('\t')
                chunk_number = cutting[1].split(' ')[0]
                start = cutting[2]
                end = cutting[3]

                seq_list.append(dictChunks[chunk_number].seq[int(start)-1:int(end)-1])

                # Coordinates according to genome
                cuttingCoordchr = textCoordchr[i].split('\t')
                path_numberCoordchr = cuttingCoordchr[0]
                chrCoordchr = cuttingCoordchr[1]
                startCoordchr = cuttingCoordchr[2]
                endCoordchr = cuttingCoordchr[3]

                seqCoordChr_list.append(dictGenome[chrCoordchr].seq[int(startCoordchr)-1:int(endCoordchr)-1])

            self.assertEqual(seq_list, seqCoordChr_list)
            
            Text.close()
            TextCoordchr.close()


if __name__ == "__main__":
    unittest.main()
