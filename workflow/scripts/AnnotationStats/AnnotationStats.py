#!/usr/bin/env python

import argparse
import logging
import os
import pandas as pd
from Bio import SeqIO

from getCumulLengthFromTEannot import getCumulLengthFromTEannot
from Stat import Stat
from AnnotationStatsWriter import AnnotationStatsWriter



class AnnotationStats(object):

    def __init__(self, seqTableName = "", pathTableName = "", genomeLength = 0, statsFileName = "", globalStatsFileName = ""):
        
        # Inputs
        self._genomeFilename = ""
        self._TElibrary = ""
        self._pathFilename = ""

        # Outputs
        self._statsFileName = ""
        self._globalStatsFileName = ""
        
        self._logFilename = ""

        self.df_path = pd.DataFrame()
        self.importPathFile()

   
    def setAttributesFromCmdLine(self):
        parser = argparse.ArgumentParser(usage="AnnotationStats.py [options]",
                                         description="Compute annotation statistics per TE.",
                                         epilog="z")

        parser.add_argument('-genome', '--genomeFasta', help= 'Genome file at fasta format', required= True, metavar='file')
        parser.add_argument('-TElibrary', '--TElibraryFasta', help='TE library (e.g. from TEdenovo) at fasta format', required=True, metavar='file')
        parser.add_argument('-path', '--pathFilename', help='Path filename from Matcher', required=True, metavar='file')
        parser.add_argument('-annotStats', '--annotStatsPerTE', help='Annotation statistics output per TE consensus', required=True, metavar='file')
        parser.add_argument('-globalAnnotStats', '--globalAnnotStatsPerTE', help='Global annotation statistics output', required=True, metavar='file')
        parser.add_argument('-statType', '--typeOfStatistics', help='Type of statistics', required=True)

        args=parser.parse_args()
        self._setAttributesFromArguments(args)


    def _setAttributesFromArguments(self, args):
        self._genomeFilename = args.genomeFasta
        self._TElibrary = args.TElibraryFasta
        self._pathFilename = args.pathFilename

        self._statsFileName = args.annotStatsPerTE
        self._globalStatsFileName = args.globalAnnotStatsPerTE

        self._statType = args.typeOfStatistics

        logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO)


    ## Import path file
    #
    def importPathFile(self):

        if os.path.exists(self._pathFilename):
            self.df_path = pd.read_csv(self._pathFilename, sep="\t", header=None, names=["path_number", "query_name", "query_start", "query_end", "subject_name", "subject_start", "subject_end", "e_value", "score", "identity"])
            cols = ['query_start', 'query_end', 'subject_start', 'subject_end', 'identity']
            self.df_path[cols] = self.df_path[cols].round(3)


    ## Retrieve all the distinct accession names in a list.
    #
    def getAccessionsList(self):

        l = []
        with open(self._TElibrary, 'r') as text:
            for line in text:
                if line[0] == ">":
                    l.append(line[1:].replace('\n', ''))

        return l


    ## Give a list of the distinct subject names present in the TE annotation output (path format)
    #
    def getDistinctSubjectList(self):

        return self.df_path["subject_name"].unique().tolist()


    ## Get the coverage of TE copies for a given family (using 'mapOp')
    #
    # @param consensus string name of a TE family/TE reference sequence ('subject_name' in the 'path' table). If empty, all subjects are considered
    # @return cumulCoverage integer cumulative coverage
    #
    # source: https://forgemia.inra.fr/urgi-anagen/repet_pipe/-/blob/dev/commons/tools/AnnotationStats.py
    def getCumulCoverage(self, consensus):
        gclft = getCumulLengthFromTEannot() # source: https://forgemia.inra.fr/urgi-anagen/repet_pipe/-/blob/dev/commons/tools/getCumulLengthFromTEannot.py
        gclft.setGenome(self._genomeFilename)
        gclft.setInputTable(self._pathFilename, self.df_path)
        gclft.setTErefseq(consensus)
        cumulCoverage, genomeLength = gclft.getCumulLength()

        return cumulCoverage, genomeLength




################################################################################################################################
# Get statistic per TE

    ## Give a list of the lengths of all paths for a given subject name
    #
    # @param subjectName string name of the subject
    # @return lPathLengths list of lengths per path
    #
    # @warning doesn't take into account the overlaps !!
    #
    def getPathLengthListFromSubject( self, subjectName ):

        df_LengthPerFragment = self.df_path.loc[self.df_path["subject_name"] == subjectName]
        #df_LengthPerFragment["lengthPerFragment"] = abs(df_LengthPerFragment["query_end"] - df_LengthPerFragment["query_start"] + 1)
        df_LengthPerFragment["lengthPerFragment"] = abs(df_LengthPerFragment.loc[:, ("query_end")] - df_LengthPerFragment.loc[:, ("query_start")] + 1)
        lLengthPerFragment = df_LengthPerFragment["lengthPerFragment"].tolist()

        return lLengthPerFragment


    ## Give a list of the length of all chains of paths for a given subject name
    #
    # @param subjectName string  name of the subject
    # @return lChainLengths list of lengths per chain of paths
    #
    # @warning doesn't take into account the overlaps !!
    #
    def getChainLengthListFromSubject( self, subjectName ):

        df_LengthPerCopy = self.df_path.loc[self.df_path["subject_name"] == subjectName]
        #df_LengthPerCopy["lengthPerCopy"] = abs(df_LengthPerCopy["query_end"] - df_LengthPerCopy["query_start"] + 1)
        df_LengthPerCopy.loc[:, ("lengthPerCopy")] = abs(df_LengthPerCopy.loc[:, ("query_end")] - df_LengthPerCopy.loc[:, ("query_start")] + 1)
        df_LengthPerCopy = df_LengthPerCopy.groupby(by="path_number").agg({'lengthPerCopy': 'sum'})

        lLengthPerCopy = df_LengthPerCopy["lengthPerCopy"].tolist()

        return lLengthPerCopy


    ## Give a list of identity of all chains of paths for a given subject name
    #
    # @param subjectName string name of the subject
    # @return lChainIdentities list of identities per chain of paths
    #
    # @warning doesn't take into account the overlaps !!
    #
    def getChainIdentityListFromSubject( self, subjectName ):
        lChainIdentities = []

        df_ChainIdentities = self.df_path.loc[self.df_path["subject_name"] == subjectName]
        df_ChainIdentities["ChainIdentities1"] = df_ChainIdentities.loc[:, ("identity")] * abs(df_ChainIdentities.loc[:, ("query_end")] - df_ChainIdentities.loc[:, ("query_start")] + 1)
        df_ChainIdentities["ChainIdentities2"] = abs(df_ChainIdentities.loc[:, ("query_end")] - df_ChainIdentities.loc[:, ("query_start")] + 1)

        df_ChainIdentities = df_ChainIdentities.groupby(by="path_number").agg({'ChainIdentities1': 'sum', 'ChainIdentities2': 'sum'})
        
        if not df_ChainIdentities.empty:
            df_ChainIdentities["ChainIdentities"] = df_ChainIdentities.loc[:, ("ChainIdentities1")] / df_ChainIdentities.loc[:, ("ChainIdentities2")]
            for i in df_ChainIdentities["ChainIdentities"]:
                lChainIdentities.append(round( float( i), 2 ))

        #sqlCmd = "SELECT SUM(identity*(ABS(query_start-query_end)+1)) / "
        #"SUM(ABS(query_end-query_start)+1) FROM {} WHERE subject_name='{}' GROUP BY PATH" .format ( self._table, subjectName )
        #self._iDb.execute( sqlCmd )
        #res = self._iDb.fetchall() # from commons/core/sql/DbMySql.py
        #for i in res:
        #    if i[0] != None:
        #        lChainIdentities.append( round( float( i[0] ), 2 ) )
        
        return lChainIdentities


    ## Retrieve the length of a sequence given its name.
    #
    # @param accession name of the sequence
    # @return seqLength integer length of the sequence
    # 
    # source: https://forgemia.inra.fr/urgi-anagen/repet_pipe/-/blob/dev/commons/core/sql/TableSeqAdaptator.py
    def getSeqLengthFromAccession(self, accession):

        d_TElibrary= SeqIO.to_dict(SeqIO.parse(self._TElibrary, "fasta"))
        return len(d_TElibrary[accession].seq)


    ## Get the number of full-lengths (95% <= L =< 105%)
    #
    # @param consensusLength integer
    # @param lLengths list of integers
    # @return fullLengthConsensusNb integer
    #
    def getNbFullLengths(self, consensusLength, lLengths):
        fullLengthConsensusNb = 0
        for i in lLengths:
            if i / float(consensusLength) >= 0.95 and i / float(consensusLength) <= 1.05:
                fullLengthConsensusNb += 1
        return fullLengthConsensusNb


    def statsForIdentityAndLength(self, dStat, lLengthPerCopy, lIdentityPerCopy):
        for i in lIdentityPerCopy:
            dStat["statsIdentityPerChain"].add(i)
        lLengthPercPerCopy = []
        for i in lLengthPerCopy:
            dStat["statsLengthPerChain"].add(i)
            lperc = 100 * i / float(dStat["length"])
            lLengthPercPerCopy.append(lperc)
            dStat["statsLengthPerChainPerc"].add(lperc)


# TODO: copies flagged as full-length fragment CAN'T belong to multi-fragments copies (fragments that have been joined). Use the beginning of implementation commented below.
    def getStatPerTE(self, consensusName):

        dConsensusStats = {}
        
        # Source: https://forgemia.inra.fr/urgi-anagen/repet_pipe/-/blob/dev/commons/core/sql/TablePathAdaptator.py
        lLengthPerFragment = self.getPathLengthListFromSubject(consensusName)
        lLengthPerCopy = self.getChainLengthListFromSubject(consensusName)
        lIdentityPerCopy = self.getChainIdentityListFromSubject(consensusName)
        
        dConsensusStats["Wcode"] = None
        dConsensusStats["length"] = self.getSeqLengthFromAccession(consensusName)
        dConsensusStats["cumulCoverage"], genomeLength = self.getCumulCoverage(consensusName)
        dConsensusStats["nbFragments"] = len(lLengthPerFragment)
        dConsensusStats["nbFullLengthFragments"] = self.getNbFullLengths(dConsensusStats["length"], lLengthPerFragment)
        dConsensusStats["nbCopies"] = len(lLengthPerCopy)
        dConsensusStats["nbFullLengthCopies"] = self.getNbFullLengths(dConsensusStats["length"], lLengthPerCopy)
        
        dConsensusStats["statsIdentityPerChain"] = Stat()
        dConsensusStats["statsLengthPerChain"] = Stat()
        dConsensusStats["statsLengthPerChainPerc"] = Stat()
        self.statsForIdentityAndLength(dConsensusStats, lLengthPerCopy, lIdentityPerCopy)
        
        return dConsensusStats

################################################################################################################################




    def run(self):
        
        logging.info("START")

        self.importPathFile()

        iASW = AnnotationStatsWriter()

        with open(self._statsFileName, "w") as fStats:
            string = "Wcode\tTE\tlength\tcovg\tfrags\tfullLgthFrags\tcopies\tfullLgthCopies\t"
            string += "meanId\tsdId\tminId\tq25Id\tmedId\tq75Id\tmaxId\t" # 7 items
            string += "meanLgth\tsdLgth\tminLgth\tq25Lgth\tmedLgth\tq75Lgth\tmaxLgth\t" # 7 items
            string += "meanLgthPerc\tsdLgthPerc\tminLgthPerc\tq25LgthPerc\tmedLgthPerc\tq75LgthPerc\tmaxLgthPerc\n" # 7 items
            fStats.write(string)

            lNamesTErefseq = self.getAccessionsList()
            lDistinctSubjects = self.getDistinctSubjectList()

            totalCumulCoverage, genomeLength = self.getCumulCoverage("")

            with open(self._globalStatsFileName, "w") as fG:
                fG.write("{}\n" .format(iASW.printResume(lNamesTErefseq, lDistinctSubjects, totalCumulCoverage, genomeLength)))

                if self._statType == "Long": 
                    for consensusName in lNamesTErefseq:
                        logging.info("processing '{}'..." .format(consensusName))
                        dStatForOneConsensus = self.getStatPerTE(consensusName)
                        iASW.addCalculsOfOneTE(dStatForOneConsensus)
                        fStats.write("{}\n" .format(iASW.getStatAsStringForTE(consensusName, dStatForOneConsensus)))
                    fG.write(iASW.printStatsForAllTEs(len(lNamesTErefseq)))

        logging.info("END")




if __name__ == "__main__":

    annotStats = AnnotationStats()
    annotStats.setAttributesFromCmdLine()
    annotStats.run()
