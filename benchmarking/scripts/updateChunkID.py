#!/usr/bin/env python

import argparse

class UpdateChunkID(object):

    def __init__(self):
        
        self.input = ""


    def setAttributesFromCmdLine(self):
        parser = argparse.ArgumentParser(usage="updateChunkID.py [options]",
                                         description="Update chunk IDs, RM and BLR outputs need same chunk ID.",
                                         epilog="z")

        parser.add_argument('-i', '--input', help= 'Align file', required= True, metavar='file')
        
        args=parser.parse_args()
        self._setAttributesFromArguments(args)


    def _setAttributesFromArguments(self, args):
        self.input = args.input


    def  run(self):

        with open(self.input, 'r') as text_input, open(self.input+".updateIDs", 'w') as text_output:

            for line in text_input:
                chunk_name = line.split('\t')[0]
                new_chunk_name = chunk_name.split(' ')[0]

                others_features = line.split('\t')[1:]

                features = [new_chunk_name] + others_features

                text_output.write("\t".join(features))

        text_input.close()
        text_output.close()


if __name__ == '__main__':
    i = UpdateChunkID()
    i.setAttributesFromCmdLine()
    i.run()
