#!/usr/bin/env python

import os
import logging
import argparse

#from AlignUtils import AlignUtils
from Align import Align

logging.basicConfig(filename='UpdateScoreOfMatches.log', filemode='w', format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO)

class UpdateScoreOfMatches( object ):
    
    def __init__(self):
        self._inputData = ""
        self._formatData = ""
        self._outputData = ""
        
    def setAttributesFromCmdLine(self):
        parser = argparse.ArgumentParser(usage="UpdateScoreOfMatches.py [options]",
                                         description="the new score is the length on the query times the percentage of identity")

        parser.add_argument('-i', '--input', help= 'input data (can be file or table)', required= True, metavar='file')
        parser.add_argument('-f', '--format', help= 'format of the data (align/path)', required= True, metavar='file')
        args=parser.parse_args()
        self._setAttributesFromArguments(args)


    def _setAttributesFromArguments(self, args):
        self._inputData = args.input
        if not os.path.exists(self._inputData):
            logging.warning("No input file (can be file or table).")
        else:
            self._outputData = self._inputData + ".newScores"
        self._formatData = args.format            

    ## Update the scores of each match in the input file
    #
    # @note the new score is the length on the query times the percentage of identity
    #
    def updateScoresInFile(self, inFile, outFile):

        inHandler = open( inFile, "r" )
        outHandler = open( outFile, "w" )
        iAlign = Align()
        
        while True:
            line = inHandler.readline()
            if line == "":
                break
            iAlign.reset()
            iAlign.setFromString( line, "\t" )
            iAlign.updateScore()
            iAlign.write( outHandler )
            
        inHandler.close()
        outHandler.close()

    def run(self):
        logging.info("START UpdateScoreOfMatches.py")

        if self._formatData == "align":
            self.updateScoresInFile( self._inputData,
                                            self._outputData )

        elif self._formatData == "path":
            pass
            
        logging.info("END UpdateScoreOfMatches.py")
    

if __name__ == "__main__":
    i = UpdateScoreOfMatches()
    i.setAttributesFromCmdLine()
    i.run()